﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    class SerializationClass
    {
        internal void Serialize(string path, BindingList<TaskInfo> tasksSource)
        {
            FileStream fs = File.Create(path);
            BinaryFormatter formatter = new BinaryFormatter();
            try
            {
                formatter.Serialize(fs, tasksSource);
            }
            catch (SerializationException)
            {
                MessageBox.Show("Failed to serialize");
            }
            finally
            {
                fs.Close();
            }
        }
        internal void Deserialize(string path, ref BindingList<TaskInfo> ReftoTasksSource)
        {
            FileStream fs = new FileStream("Serialize.dat", FileMode.Open);
            BindingList<TaskInfo> tasksSource = new BindingList<TaskInfo>();
            BinaryFormatter formatter = new BinaryFormatter();
            try
            {
                tasksSource = (BindingList<TaskInfo>)formatter.Deserialize(fs);
                ReftoTasksSource = tasksSource;
            }
            catch (DeserializationException)
            {
                MessageBox.Show("Failed to deserialize");
            }
            finally
            {
                fs.Close();
            }
        }
    }
}
