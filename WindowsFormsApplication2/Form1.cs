﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;
using System.Net;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        private BindingList<TaskInfo> tasksSource;
        public Form1()
        {
            InitializeComponent();
            tasksSource = TaskInfo.loadTasksFromDatabase();
            lbTaskList.DataSource = tasksSource;

        }

        private void label2_Click(object sender, EventArgs e)
        {
        }

        private void bSend_Click(object sender, EventArgs e)
        {
            TaskInfo task;
            if (tabControl1.SelectedTab==tabPage1)
            {
               task = new ImageInfo(tbSearchingText.Text, tbURL.Text, tbTask.Text, tbEmail.Text);
            }
            else
            {
                task = new MyWeatherInfo(tbCity.Text, Convert.ToInt32(numeric.Value), tbEmail.Text,tbTask.Text);
            }
            if(tabControl2.SelectedTab==tabPage3)
            {
                task.setAction(new EmailSendAction());
            }
            else if (tabControl2.SelectedTab == tabPage4)
            {
                task.setAction(new ShowTaskResultAction());
            }

            tasksSource.Add(task);
            task.addToDatabase();
        }

        private void tbSearchingText_TextChanged(object sender, EventArgs e)
        {

        }

        private void tbURL_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach( TaskInfo task in tasksSource)
            {
                try
                {
                    task.run();            
                }
                catch (PictureNotFoundException)
                {
                    MessageBox.Show("Picture Not Found");
                }
                catch (DownloadStreamProblemException)
                {
                    MessageBox.Show("Problem with downloading picture");
                }           
                catch (CreatingMessageException)
                {
                    MessageBox.Show("Problem with creating message");
                }
                catch (SendingMessageException)
                {
                    MessageBox.Show("Problem with sending Message");
                }
                catch (APIDownloadException)
                {
                    MessageBox.Show("Problem with downloading API, probably you got banned");
                }
                

                FileStream file = new FileStream("log.txt", FileMode.Append, FileAccess.Write);
                try
                {
                    StreamWriter sw = new StreamWriter(file);
                    sw.WriteLine(task);
                    sw.Close();
                }
                catch (LogCreateProblemException)
                {
                    MessageBox.Show("Error in writting log");
                }
            }
        }

        private void bClean_Click(object sender, EventArgs e)
        {
            tasksSource.Clear();
            using (var ctx = new TasksDB())
            {
                ctx.clearImage();
            }
        }

        private void bSerialize_Click(object sender, EventArgs e)
        {
            SerializationClass serialize = new SerializationClass();
            serialize.Serialize("Serialize.dat", tasksSource);
        }

        private void bDeserialize_Click(object sender, EventArgs e)
        {
            SerializationClass deserialize = new SerializationClass();
            deserialize.Deserialize("Serialize.dat", ref tasksSource);
            lbTaskList.DataSource = tasksSource;
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void pogodaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var weatherBox = new Form2();
            weatherBox.Show();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void tbTask_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
