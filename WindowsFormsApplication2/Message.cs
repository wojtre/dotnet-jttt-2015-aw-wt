﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
using System.Windows.Forms;
using System.Net.Mime;

namespace WindowsFormsApplication2
{
    class Message
    {
        private MailMessage message = new MailMessage();

        public void CreateMessagewithAttachment(String emailAddress, String filePath,
            String subject, String body)
        {
            try
            {
                Attachment imageAttachment =
                new Attachment(filePath, MediaTypeNames.Application.Octet);
                message.To.Add(new MailAddress(emailAddress));
                message.From = new MailAddress("janinakoziwas@gmail.com");
                message.Subject = subject;
                message.Body = body;
                message.Attachments.Add(imageAttachment);
                
            }
            catch(Exception)
            {
                throw new CreatingMessageException();
            }
        }

        public void CreateMessage(String emailAddress, String subject, String body)
        {
            try
            {
                message.To.Add(new MailAddress(emailAddress));
                message.From = new MailAddress("janinakoziwas@gmail.com");
                message.Subject = subject;
                message.Body = body;
            }
            catch (Exception)
            {
                throw new CreatingMessageException();
            }
        }

        public void SendMessage()
        {

            SmtpClient smtpClient = new SmtpClient();
            NetworkCredential Credential = new NetworkCredential("janinakoziwas@gmail.com","ADMIN123");

            smtpClient.Host = "smtp.gmail.com";
            smtpClient.Port = 587;
            smtpClient.UseDefaultCredentials= false;
            smtpClient.Credentials = Credential;
            smtpClient.EnableSsl = true;
            try
            {
                smtpClient.Send(message);
            }catch(Exception)
            {
                throw new SendingMessageException();
            }
        }


    }
}