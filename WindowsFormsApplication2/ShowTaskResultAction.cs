﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication2
{
    public class ShowTaskResultAction : Action
    {
                public ShowTaskResultAction()
        {
            actionName = "Show";
        }

                public override void doAction(TaskInfo info)
                {
                    var TaskResultBox = new Form3();
                    TaskResultBox.SetPictureBox(info.PictureAddres());
                    TaskResultBox.SetDescription(info.printDescription());
                    TaskResultBox.Show(); 
                }
    }
}
