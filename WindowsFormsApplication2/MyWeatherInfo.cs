﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication2
{
    public class Coord
    {
        public double lon { get; set; }
        public double lat { get; set; }
    }

    public class Sys
    {
        public double message { get; set; }
        public string country { get; set; }
        public int sunrise { get; set; }
        public int sunset { get; set; }
    }

    public class Weather
    {
        public int id { get; set; }
        public string main { get; set; }
        public string description { get; set; }
        public string icon { get; set; }
    }

    public class Main
    {
        public double temp { get; set; }
        public double temp_min { get; set; }
        public double temp_max { get; set; }
        public double pressure { get; set; }
        public double sea_level { get; set; }
        public double grnd_level { get; set; }
        public int humidity { get; set; }
    }

    public class Wind
    {
        public double speed { get; set; }
        public double deg { get; set; }
    }

    public class Clouds
    {
        public int all { get; set; }
    }

    public class WeatherInfo
    {
        public Coord coord { get; set; }
        public Sys sys { get; set; }
        public List<Weather> weather { get; set; }
        public string @base { get; set; }
        public Main main { get; set; }
        public Wind wind { get; set; }
        public Clouds clouds { get; set; }
        public int dt { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public int cod { get; set; }
    }
    [Serializable]
    public class MyWeatherInfo : TaskInfo
    {
        WeatherInfo info;
        public virtual string cityName { get; set; }
        public virtual int? demandTemp{get;set;}
        public override void run()
        {
            DownloadWeatherInfo();
            if (demandTemp < info.main.temp - 273.15)
            {
                act.doAction(this);
            }
        }

        public MyWeatherInfo() {
            info = new WeatherInfo();
        }
        public MyWeatherInfo(string city)
        {
            cityName = city;
        }

        public MyWeatherInfo(string cityName, int temp, string email, string taskName)
        {
            this.cityName = cityName;
            demandTemp = temp;
            emailAddress = email;
            this.taskName = taskName;
            info = new WeatherInfo();
        }

        public void DownloadWeatherInfo()
        {
            string json;
            try
            {
                using (var wc = new WebClient())
                {
                    json = wc.DownloadString("http://api.openweathermap.org/data/2.5/weather?q=" + cityName);
                }
                info = JsonConvert.DeserializeObject<WeatherInfo>(json);
            }
            catch (Exception)
            {
                throw new APIDownloadException();
            }
        }

        override public String ToString()
        {
            return "Task Name: " + taskName + " Action:  " + act.actionName +" Demand Temp: " + demandTemp + " City: " + cityName+" Email: "+emailAddress;
        }

        public override string printDescription()
        {
            string returningString;
            if (info.weather.Count>0)
            {
                returningString =
                "dzisiaj jest " + (info.main.temp - 273.15) + " stopni Celsjusza. Ciśnienie " + info.main.pressure + " hPa. "
                + " A co na niebie? " + info.weather[0].description;
            }
            else
                returningString = "No info about current Weather State";
            return returningString;
        }
           
        public string ReturnIconURL()
        {
            string iconURL;
            iconURL = "http://openweathermap.org/img/w/" + info.weather[0].icon + ".png";
            return iconURL;
        }

        public override string PictureAddres()
        {
            string iconURL;
            iconURL = "http://openweathermap.org/img/w/" + info.weather[0].icon + ".png";
            return iconURL;
        }

        override public void addToDatabase()
        {
            using (var ctx = new TasksDB())
            {
                ctx.Task.Add(this);
                ctx.SaveChanges();
            }
        }

        override public void Send()
        {
            Message message = new Message();
            String subject = "Weather";
            String body = printDescription();
            message.CreateMessage(emailAddress, subject, body);
            message.SendMessage();
        }
    }
}
