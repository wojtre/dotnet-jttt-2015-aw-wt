﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication2
{
    public class TasksDB : DbContext
    {
        public TasksDB() : base("Zbior danych o obrazkach")
        {
        }
        public DbSet<TaskInfo> Task { get; set; }

        public void clearImage()
        {
                Task.RemoveRange(Task);
                SaveChanges();
        }
    }
}
