﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void cityName_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void bweather_Click(object sender, EventArgs e)
        {
            var weather = new MyWeatherInfo(tbCity.Text);
            weather.DownloadWeatherInfo();
            tbweather.Text = weather.printDescription();
            pictureBox1.Load(weather.ReturnIconURL());
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void tbCity_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
