﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication2
{
    public abstract class Action
    {
        public String actionName="";
        public abstract void doAction(TaskInfo info);
    }
}
