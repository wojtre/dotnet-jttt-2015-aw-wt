﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication2
{
    class PictureNotFoundException : System.Exception { }
    class DownloadStreamProblemException : System.Exception { }
    class LogCreateProblemException : System.Exception { }
    class SendingMessageException : System.Exception { }
    class CreatingMessageException : System.Exception { }
    class SerializationException : System.Exception { }
    class DeserializationException : System.Exception { }
    class APIDownloadException : System.Exception { }
}
