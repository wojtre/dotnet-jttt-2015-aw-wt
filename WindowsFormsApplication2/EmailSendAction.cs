﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication2
{
    public class EmailSendAction : Action
    {
        public EmailSendAction()
        {
            actionName = "Sending email";
        }
        public override void doAction(TaskInfo info)
        {
            info.Send();
        }
    }
}
