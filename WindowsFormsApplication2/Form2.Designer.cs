﻿namespace WindowsFormsApplication2
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbweather = new System.Windows.Forms.RichTextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cityName = new System.Windows.Forms.Label();
            this.bweather = new System.Windows.Forms.Button();
            this.tbCity = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tbweather
            // 
            this.tbweather.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbweather.Location = new System.Drawing.Point(31, 203);
            this.tbweather.Name = "tbweather";
            this.tbweather.Size = new System.Drawing.Size(317, 96);
            this.tbweather.TabIndex = 1;
            this.tbweather.Text = "";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(392, 203);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(125, 96);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // cityName
            // 
            this.cityName.AutoSize = true;
            this.cityName.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cityName.Location = new System.Drawing.Point(51, 42);
            this.cityName.Name = "cityName";
            this.cityName.Size = new System.Drawing.Size(102, 31);
            this.cityName.TabIndex = 5;
            this.cityName.Text = "Miasto:";
            this.cityName.Click += new System.EventHandler(this.cityName_Click);
            // 
            // bweather
            // 
            this.bweather.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bweather.Location = new System.Drawing.Point(31, 113);
            this.bweather.Name = "bweather";
            this.bweather.Size = new System.Drawing.Size(486, 71);
            this.bweather.TabIndex = 6;
            this.bweather.Text = "Jaka jest pogoda?";
            this.bweather.UseVisualStyleBackColor = true;
            this.bweather.Click += new System.EventHandler(this.bweather_Click);
            // 
            // tbCity
            // 
            this.tbCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbCity.Location = new System.Drawing.Point(159, 42);
            this.tbCity.Name = "tbCity";
            this.tbCity.Size = new System.Drawing.Size(334, 31);
            this.tbCity.TabIndex = 7;
            this.tbCity.Text = "";
            this.tbCity.TextChanged += new System.EventHandler(this.tbCity_TextChanged);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(540, 311);
            this.Controls.Add(this.tbCity);
            this.Controls.Add(this.bweather);
            this.Controls.Add(this.cityName);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.tbweather);
            this.Name = "Form2";
            this.Text = "Form2";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox tbweather;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label cityName;
        private System.Windows.Forms.Button bweather;
        private System.Windows.Forms.RichTextBox tbCity;

    }
}