namespace WindowsFormsApplication2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class refactorr : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.ImageInfoes", newName: "TaskInfoes");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.TaskInfoes", newName: "ImageInfoes");
        }
    }
}
