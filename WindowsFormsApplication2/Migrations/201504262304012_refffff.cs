namespace WindowsFormsApplication2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class refffff : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TaskInfoes", "cityName", c => c.String());
            AddColumn("dbo.TaskInfoes", "demandTemp", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TaskInfoes", "demandTemp");
            DropColumn("dbo.TaskInfoes", "cityName");
        }
    }
}
