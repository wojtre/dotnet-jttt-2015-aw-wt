namespace WindowsFormsApplication2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class reff : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TaskInfoes", "Discriminator", c => c.String(nullable: false, maxLength: 128));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TaskInfoes", "Discriminator");
        }
    }
}
