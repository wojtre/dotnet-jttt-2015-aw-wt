namespace WindowsFormsApplication2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class refff : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TaskInfoes", "actionName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TaskInfoes", "actionName");
        }
    }
}
