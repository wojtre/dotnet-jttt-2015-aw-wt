namespace WindowsFormsApplication2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dodaniegetow : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ImageInfoes", "Keyword", c => c.String());
            AddColumn("dbo.ImageInfoes", "WebsiteURL", c => c.String());
            AddColumn("dbo.ImageInfoes", "TaskName", c => c.String());
            AddColumn("dbo.ImageInfoes", "EmailAddress", c => c.String());
            DropColumn("dbo.ImageInfoes", "ImageAddress");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ImageInfoes", "ImageAddress", c => c.String());
            DropColumn("dbo.ImageInfoes", "EmailAddress");
            DropColumn("dbo.ImageInfoes", "TaskName");
            DropColumn("dbo.ImageInfoes", "WebsiteURL");
            DropColumn("dbo.ImageInfoes", "Keyword");
        }
    }
}
