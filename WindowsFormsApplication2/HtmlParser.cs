﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Net;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public class HtmlParser
    {
        
        private string url;

        public HtmlParser(string url)
        {
            this.url = url;
        }

        public string GetPageHtml()
        {
            using (WebClient wc = new WebClient())
            {
                byte[] data = wc.DownloadData(url);
                string html = System.Net.WebUtility.HtmlDecode(Encoding.UTF8.GetString(data));
                return html;
            }
        }

        public String ReturnImageWithKeywordAddress(String keyword)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();

            string pageHtml = GetPageHtml();
            doc.LoadHtml(pageHtml);
            var nodes = doc.DocumentNode.Descendants("img");
            foreach (var node in nodes)
            {
                if(node.GetAttributeValue("alt","").ToString().Contains(keyword))
                {
                   return node.GetAttributeValue("src", "");
                }
            }
            return "";

        }
    }
}
