﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication2
{
    public abstract class TaskInfo
    {
        public int Id { get; set; }
        protected Action act;
        public virtual string actionName { get; set; }
        public String emailAddress = "";
        public String taskName;
        public abstract void Send();
        public abstract void addToDatabase();
        public abstract string printDescription();

        public abstract string PictureAddres();
        public virtual String TaskName
        {
            get
            {
                return taskName;
            }
            set
            {
                taskName = value;
            }
        }
        public virtual String EmailAddress
        {
            get
            {
                return emailAddress;
            }
            set
            {
                emailAddress = value;
            }
        }
        public static BindingList<TaskInfo> loadTasksFromDatabase()
        {
            var taskSource = new BindingList<TaskInfo>();
            using (var db = new TasksDB())
            {
                foreach (var task in db.Task)
                {
                    if(String.Compare(task.actionName,"Sending email").Equals(0))
                        task.setAction(new EmailSendAction());
                   if (task.actionName.Equals("Show"))
                        task.setAction(new ShowTaskResultAction());

                    taskSource.Add(task);
                }
            }
            return taskSource;
        }
        public void setAction(Action a) { act = a; actionName = a.actionName; }
        public abstract void run();
    }
}
