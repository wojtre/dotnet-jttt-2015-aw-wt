﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    [Serializable]
    public class ImageInfo : TaskInfo
    {
       public String imageAddress = "";
       public String imageName = "";
       public String keyword="";
       public String websiteURL="";
       
       public static int picNumber = 0;

        public virtual String Keyword
        {
            get
            {
                return keyword;
            }
            set
            {
                keyword = value;
            }
        }
        public virtual String WebsiteURL
        {
            get
            {
                return websiteURL;
            }
            set
            {
                websiteURL = value;
            }
        }




        public ImageInfo() {
            ;
        }

        public ImageInfo(String keyword, String websiteURL, String taskName, String emailAddress)
        {
            this.keyword = keyword;
            this.websiteURL = websiteURL;
            this.taskName = taskName;
            this.emailAddress = emailAddress;
            taskName = "Demotywatory";
        }

        public void Download()
        {
            WebClient client = new WebClient();
            HtmlParser toParse = new HtmlParser(websiteURL);
            imageAddress =
                toParse.ReturnImageWithKeywordAddress(keyword);

            if ((imageAddress.Length > 0) && imageAddress[0] == '/')
                imageAddress = websiteURL + imageAddress;

           if (imageAddress.Length == 0)
           {
               throw new PictureNotFoundException();
           }
           else
           {
               try
               {
                   Stream stream = client.OpenRead(imageAddress);

                   Bitmap bitmap = new Bitmap(stream); // Error : Parameter is not valid.
                   stream.Flush();
                   stream.Close();
                   if (bitmap != null)
                   {
                       imageName += picNumber + ".jpg";
                       picNumber++;
                       bitmap.Save(imageName);
                   }
               }
               catch
               {
                   throw new DownloadStreamProblemException();
               }
            }         
        }

        override public void Send()
        {
            Message message = new Message();
            String subject ="I have a picture for you :)";
            String body = "I've just found picture with keyword: " + keyword;
            message.CreateMessagewithAttachment(emailAddress, imageName, subject, body);
            message.SendMessage();
        }
       
        override public String ToString()
        {
            return "Task Name: " + taskName + " Action:  " + act.actionName + " Website: " + websiteURL + " Keyword: " + keyword;
        }


        override public void addToDatabase()
        {
            using (var ctx = new TasksDB())
            {
                ctx.Task.Add(this);
                ctx.SaveChanges();
            }
        }
        public override void run()
        {
            Download();
            act.doAction(this);

        }
        public override string printDescription()
        {
            return ToString();
        }
        public override string PictureAddres()
        {
            return imageAddress;
        } 
    }
}
