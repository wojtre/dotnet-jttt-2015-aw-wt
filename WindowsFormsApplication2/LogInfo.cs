﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    class LogInfo
    {
        public void SaveLog(string path, ImageInfo loginfo)
        {
            FileStream file = new FileStream(path, FileMode.Append, FileAccess.Write);
             try
                {               
                    StreamWriter sw = new StreamWriter(file);
                    sw.WriteLine(loginfo);
                }
                catch (Exception)
                {
                    throw new LogCreateProblemException();
                }
             file.Close();
        }
    }
}